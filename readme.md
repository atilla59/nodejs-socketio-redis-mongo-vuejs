# NodeJS, MongoDB, SocketIO, Nginx, VueJs


## Project Run
Build docker images (nginx,nodejs,redis,mongo)
```sh
docker-compose up
```

### Run UI (Vuejs App - SPA)

```sh
cd ./front 
npm run dev
```

## System Design

### - Backend
- backend api written in nodejs and using mongo for database and usign redis for pub/sub mechanism because if server running multiple server,
socketio require communication other websocket server so redis pub/sub provides this communication
- nginx running on 80 port and it has upstream for sticky session so i am enabled "ngx_http_upstream_consistent_hash"
- i am using [Pm2](https://pm2.keymetrics.io/)  for process management 
- redis for cache and pub/sub mechanism running on 6379
- mongo running on 27017
- Some keywords (express,socketio,jwt,bcrypt,passport,express-validator,mongoose,redis)

#### backend endpoints
- POST /register
- GET /api/user/:id (Require Auth via Bearer JWT Token)
- POST /api/user/login
- GET /api/users (Require Auth via Bearer JWT Token)

### - Front
- written in VueJS front end framework and using socketio-client


### Sticky session explanation
![alt text](https://www.imperva.com/learn/wp-content/uploads/sites/13/2019/01/session-stickiness-diagram.jpg)

### socketio redis adapter for multiple websocket server communicate with each others
![alt text](https://socket.io/images/broadcasting-redis.png)

### demo for app
![alt text](demo.gif)



