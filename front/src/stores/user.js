import {defineStore} from 'pinia'

export const useUserStore = defineStore({
    id     : 'user',
    state  : () => ({
        userData       : {},
        isAuthenticated: false,
        token          : null,
        socket         : null
    }),
    actions: {
        setUser(user, token) {
            console.log(user, token)
            this.userData = user
            this.token = token
            this.isAuthenticated = true
        },

        setSocketInstance(socket) {
            this.socket = socket
        }

    }
})
