import {createRouter, createWebHistory} from 'vue-router'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import List from '../views/List.vue'
import {useUserStore} from '../stores/user'


const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes : [
        {
            path    : '/',
            redirect: '/login'
        },
        {
            path     : '/login',
            name     : 'home',
            component: Login
        },
        {
            path     : '/register',
            name     : 'register',
            component: Register,
        },
        {
            path     : '/list',
            name     : 'list',
            component: List,
            meta     : {requiresAuth: true}
        },
        {
            path: '/:pathMatch(.*)*',
            redirect: '/login'
        }

    ]
})

router.beforeEach((to, from) => {
    // instead of having to check every route record with
    // to.matched.some(record => record.meta.requiresAuth)

    const userStore = useUserStore()

    if (to.meta.requiresAuth &&  !userStore.$state.isAuthenticated) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        return {
            path: '/login',
            // save the location we were at to come back later
            query: {redirect: to.fullPath},
        }
    }
})

export default router
