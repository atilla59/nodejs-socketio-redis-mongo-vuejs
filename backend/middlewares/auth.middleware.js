const passport = require('passport');

const AuthMiddleware = {

    auth: async (req, res, next) => {
        passport.authenticate('jwt', { session: false }, (err, user) => {
            if (err) return next(err);

            if (!user) {
                return res.status(401).json({
                    status: 'error',
                    msg: 'Forbidden',
                });
            }
            req.user = user;
            return next();
        })(req, res, next);
    },

};
module.exports = AuthMiddleware;
