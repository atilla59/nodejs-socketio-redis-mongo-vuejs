const createError = require('http-errors');
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const cors = require('cors')

const cookieParser = require('cookie-parser');
const logger = require('morgan');
const PORT = process.env.APP_PORT;

// DB
const mongoose = require("mongoose");
//init db connection
mongoose.connect(process.env.MONGODB_CONN).then(() => console.log("DB Connection Successfully")).catch((e) => console.error("Error DB CONNECTİON", e))


// PASSPORT JWT
const passport = require("passport");
const {jwtStrategy} = require("./config/passport");




const AppRoutes = require('./routes/index');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(passport.initialize());
app.use(cors())
passport.use('jwt', jwtStrategy);


app.use('/api', AppRoutes);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // render the error page
    console.error(err)
    res.json(err.status || 500);
});

//socket io
const {startSocketServer} = require('./socket/socket')
const io = startSocketServer(server)

server.listen(PORT, () => {
    console.log('Server listening at port %d', PORT);
})

module.exports.socketIO = io








