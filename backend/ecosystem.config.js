module.exports = {
    apps: [{
        name        : "nodejs-socketio",
        script      : "./app.js",
        instances   : '1',
        exec_mode   : 'cluster',
        watch       : ['routes', 'controllers','app.js','models','validations','config','utils','socket'],
        env         : {
            "MONGODB_CONN": "mongodb://app-mongo",
            "NODE_ENV": "development",
            "APP_PORT":3000,
            "JWT_SECRET":"some-jwt-secret"
        },
        watch_delay : 500,
        ignore_watch: [
            'node_modules/'
        ],
        error_file  : 'pm2-logs/err.log',
        out_file    : 'pm2-logs/out.log',
        log_file    : 'pm2-logs/combined.log',
    }]
}
