const jwt = require('jsonwebtoken');
const moment = require('moment');


function generateToken(userId, expires, type, secret = process.env.JWT_SECRET) {
    const payload = {
        sub: userId,
        iat: moment().unix(),
        exp: expires.unix(),
        type,
    };
    return jwt.sign(payload, secret);
}

const tokenUtils = {

    generateAuthTokens: async (user) => {
        const accessTokenExpires = moment().add(60*3, 'minutes');
        const accessToken = generateToken(user.id, accessTokenExpires, "access");

        const refreshTokenExpires = moment().add(10, 'days');
        const refreshToken = generateToken(user.id, refreshTokenExpires, "refresh");

        return {
            access : {
                token  : accessToken,
                expires: accessTokenExpires.toDate(),
            },
            refresh: {
                token  : refreshToken,
                expires: refreshTokenExpires.toDate(),
            },
        };
    }
}

module.exports = tokenUtils
