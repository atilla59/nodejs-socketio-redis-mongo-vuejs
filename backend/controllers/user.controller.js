const User = require("../models/user.model");
const {tokenUtils} = require("../utils/index");

const App = require('./../app')


const userController = {

    store: async (req, res) => {
        try {

            let body = req.body;

            if (await User.isEmailTaken(body.email)) {
                return res.status(400).json({status: "error", message: 'Email already taken'})
            }

            const savedUser = await User.create(body);
            const tokens = await tokenUtils.generateAuthTokens(savedUser)


            App.socketIO.emit("newRegistrationEvent", savedUser)

            return res.status(200).json({
                status: 'success',
                data  : savedUser,
                tokens: tokens
            });
        } catch (e) {
            console.error(e);
            return res.status(500).json({status: 'error', msg: e});
        }
    },

    get: async (req, res) => {
        try {

            let user = await User.findById(req.params.id)

            if (!user) return res.status(404).json({status: 'error', msg: "not found"});

            return res.json({"status": "success", data: user})


        } catch (e) {
            console.error(e)
            return res.status(500).json({status: 'error', msg: e});
        }
    },

    login: async (req, res) => {

        try {

            let {email, password} = req.body
            const user = await User.findOne({"email": {$eq: email}});


            if (!user || !(await user.isPasswordMatch(password))) {
                return res.status(401).json({status: "error", msg: 'Incorrect email or password'});
            }
            const tokens = await tokenUtils.generateAuthTokens(user)


            return res.status(200).json({
                status: 'success',
                data  : user,
                tokens: tokens
            });


        } catch (e) {
            console.error(e)
            return res.status(500).json({status: 'error', msg: e});
        }


    },

    index: async (req, res) => {

        try {

            let users = await User.find({})

            return res.status(200).json({
                status: 'success',
                data  : users,
            });

        } catch (e) {
            console.error(e)
            return res.status(500).json({status: 'error', msg: e});
        }
    }
}

module.exports = userController

