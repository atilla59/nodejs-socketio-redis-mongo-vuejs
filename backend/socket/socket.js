// socket io
const {createAdapter} = require('@socket.io/redis-adapter');
const {createClient} = require('redis');
const {Server} = require('socket.io');
const App = require("../app");
const pubClient = createClient({url: "redis://app-redis:6379"});
const subClient = pubClient.duplicate();

let io;

function startSocketServer(server) {
    io = new Server(server, {
        cors      : {
            origins: ['*'],
            methods: ['GET', 'POST'],
        },
        transports: ['websocket'],
    })
    // for redis 4
    Promise.all([pubClient.connect(), subClient.connect()]).then(() => {
        io.adapter(createAdapter(pubClient, subClient));
    });
    io.on('connection', handleSocketConnection)
    return io
}

function handleSocketConnection(socket) {
    console.log('new socket connection', socket.id);

    socket.on('online', async (data) => {
        console.log(data)
        socket.data.userId = data.userId

        // send online users to all except who user be online
        socket.broadcast.emit("onlineEvent", {userId: data.userId});

        const sockets = await io.fetchSockets()

        let onlineUserIds = await sockets.map((socket) => socket.data.userId)


        // send online users to all who user be online
        socket.emit('onlineUsersEvent', onlineUserIds)

    })

    socket.on('newRegistration', async (data) => {
        socket.broadcast.emit("newRegistrationEvent", data);
    })

    socket.on('getOnlineUsers', async (data) => {

        const sockets = await io.fetchSockets()

        let onlineUserIds = await sockets.map((socket) => socket.data.userId)

        io.to(socket.id).emit("getOnlineUsersEvent", onlineUserIds);
    })

    socket.on("disconnect", (reason) => {
        io.emit("disconnectEvent", {userId: socket.data.userId, reason: reason})
    });
}

module.exports = {
    startSocketServer
}




