const {check, validationResult} = require('express-validator');

module.exports = {
    register: [
        check('email').isEmail().withMessage('Invalid email address!'),
        check('password').isLength({min: 8}).withMessage("Password Minimum 8 characters").notEmpty(),
        check('name').isLength({min: 2, max: 20}),
        check('surname').isLength({min: 2, max: 20}),
        check('country').notEmpty(),
        check('lang').notEmpty(),
        check('password_confirm').custom((value, {req}) => {
            if (value !== req.body.password) {
                throw new Error('Password confirmation does not match password');
            }
            return true;
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) return res.status(422).json({errors: errors.array()});
            return next();
        },
    ],

    login:[
        check('email').isEmail().withMessage('Invalid email address!'),
        check('password').notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) return res.status(422).json({errors: errors.array()});
            return next();
        },
    ]
}
