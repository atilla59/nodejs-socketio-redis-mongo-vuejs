var express = require('express');
var router = express.Router();

const {userController} = require('../controllers/index');
const {userValidation} = require('../validations/index');
const AuthMiddleWare = require('../middlewares/auth.middleware');


router.route('/register').post(userValidation.register, userController.store)
router.route('/login').post(userValidation.login, userController.login)
router.route('/').get(AuthMiddleWare.auth, userController.index)
router.route('/:id').get(AuthMiddleWare.auth, userController.get)

module.exports = router;
