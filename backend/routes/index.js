var express = require('express');
var router = express.Router();

const UserRoutes = require('./user.route')

router.use('/users',UserRoutes)

module.exports = router;
